Vue.filter('capitalize', function (value) {
  if (!value) return ''
  value = value.toString()
  return value.charAt(0).toUpperCase() + value.slice(1)
})

const app = new Vue({
	el:'#app',
	data:{
		term:'',
		results:[],
		noResults:false,
		searching:false,
		pageNum: 1
	},
	methods:{
    loadResults: function(){
			var baseUrl = this.$refs.container.dataset.apiUrl;
		  this.searching = true;
			var escapedTerm = encodeURIComponent(this.term);
			var url = `${baseUrl}?term=${escapedTerm}&page=${this.pageNum}`;
			fetch(url)
			.then(res => res.json())
			.then(collection => {
				this.searching = false;
				this.results = collection;
				this.noResults = collection.length === 0;
			});
		},

		searchEmployee: function(event) {
      event.preventDefault();
			this.pageNum = 1;
			this.loadResults();
		},


		nextPage:function(s) {
			this.pageNum++;
			this.loadResults();
		},

		prevPage:function(s) {
			if (this.pageNum > 1){
				this.pageNum--;
				this.loadResults();
			}
		}
	}
});
