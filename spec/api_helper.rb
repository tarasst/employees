require 'rails_helper'

RSpec.shared_context "api helpers", :shared_context => :metadata  do
  def json
    JSON.parse(response.body)
  end

  let(:request_headers){ { "CONTENT_TYPE" => "application/json" } }

end

RSpec.configure do |rspec|
  rspec.include_context "api helpers", :include_shared => true
end
