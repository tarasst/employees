require "api_helper"

describe 'GET /companies' do
  include_context "api helpers"

  it 'responds with 200' do
    get '/companies'
    expect(response.status).to eq(200)
  end

  it 'returns empty array' do
    get '/companies'
    expect(json).to eq([])
  end

  context 'with data' do
    before do
      Company.create!(name: 'Example GmbH')
    end

    it 'returns 1 object in array' do
      get '/companies'
      expect(json.count).to eq(1)
      expect(json.first).to include('name' => 'Example GmbH', 'id' => anything)
    end
  end
end

describe 'POST /companies' do
  include_context "api helpers"

  let(:request_body){ {name: 'Test 1'}.to_json }

  it 'responds with 201' do
    post '/companies', params: request_body, headers: request_headers
    expect(response.status).to eq(201)
  end

  it 'creates company' do
    post '/companies', params: request_body, headers: request_headers
    expect(Company.where(name: 'Test 1').count).to eq(1)
  end

  context 'when "name" parameter is missing' do
    let(:request_body){ { }.to_json }

    it 'responds with 400 and error description' do
      post '/companies', params: request_body, headers: request_headers
      expect(response.status).to eq(400)
      expect(json['message']).to match(/"name" wasn't supplied/)
    end
  end
end


describe 'PATCH /companies/id' do
  include_context "api helpers"

  let(:existing_company){ Company.create!(name: 'Example GmbH') }

  let(:request_body){ {name: 'Test 2'}.to_json }

  it 'responds with 200 and updates company name' do
    patch "/companies/#{existing_company.id}", params: request_body, headers: request_headers
    expect(response.status).to eq(200)
    expect(existing_company.reload.name).to eq('Test 2')
  end

end

describe 'DELETE /companies/id' do
  include_context "api helpers"

  let!(:existing_company){ Company.create!(name: 'Example GmbH') }

  it 'responds with 204 and deletes the company' do
    expect{
      delete "/companies/#{existing_company.id}", headers: request_headers
    }.to change{
      Company.count
    }.by(-1)
    expect(response.status).to eq(204)
  end

end
