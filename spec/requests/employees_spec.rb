require "api_helper"

describe 'GET /employees' do
  include_context "api helpers"

  it 'responds with 200' do
    get '/employees'
    expect(response.status).to eq(200)
  end

  it 'returns empty array' do
    get '/employees'
    expect(json).to eq([])
  end

  context 'with data' do

    let(:company){  Company.create!(name: 'Maschio S.R.L')   }
    let(:personal_data){  {
      'first_name' => 'Francesco',
      'last_name' => 'Zanichelli',
      'company_id' => company.id
      }
    }
    let!(:existing_employee){ Employee.create!(personal_data) }


    it 'returns 1 object in array' do
      get '/employees'
      expect(json.count).to eq(1)
      expect(json.first).to include('first_name' => 'Francesco', 'id' => anything)
    end
  end
end

describe 'POST /employees' do
  include_context "api helpers"

  let(:company){  Company.create!(name: 'Maschio S.R.L')   }
  let(:personal_data){  {
    'first_name' => 'Francesco',
    'last_name' => 'Zanichelli',
    'company_id' => company.id
    }
  }

  let(:request_body){ personal_data.to_json }

  it 'responds with 201' do
    post '/employees', params: request_body, headers: request_headers
    expect(response.status).to eq(201)
  end

  it 'creates employee' do
    post '/employees', params: request_body, headers: request_headers
    expect(Employee.where(last_name: 'Zanichelli').count).to eq(1)
  end

  context 'when "name" parameter is missing' do
    let(:request_body){ { "last_name" => "Burg" }.to_json }

    it 'responds with 400 and error description' do
      post '/employees', params: request_body, headers: request_headers
      expect(response.status).to eq(400)
      expect(json['message']).to match(/"company_id", "first_name" weren't supplied/)
    end
  end
end


describe 'PATCH /employees/id' do
  include_context "api helpers"

  let(:company){  Company.create!(name: 'Maschio S.R.L')   }
  let(:personal_data){  {
    'first_name' => 'Francesco',
    'last_name' => 'Zanichelli',
    'company_id' => company.id
    }
  }
  let!(:existing_employee){ Employee.create!(personal_data) }
  let(:updated_personal_data){
    personal_data.merge('last_name' => 'Zingarelli')
  }


  let(:request_body){ updated_personal_data.to_json }

  it 'responds with 200 and updates employee name' do
    patch "/employees/#{existing_employee.id}", params: request_body, headers: request_headers
    expect(response.status).to eq(200)
    expect(existing_employee.reload.last_name).to eq('Zingarelli')
  end

end

describe 'DELETE /employees/id' do
  include_context "api helpers"

  let(:company){  Company.create!(name: 'Maschio S.R.L')   }
  let(:personal_data){  {
    'first_name' => 'Francesco',
    'last_name' => 'Zanichelli',
    'company_id' => company.id
    }
  }
  let!(:existing_employee){ Employee.create!(personal_data) }

  it 'responds with 204 and deletes the employee' do
    expect{
      delete "/employees/#{existing_employee.id}", headers: request_headers
    }.to change{
      Employee.count
    }.by(-1)
    expect(response.status).to eq(204)
  end

end
