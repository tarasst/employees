class EmployeesController < ApplicationController
  before_action :set_employee, only: [:show, :update, :destroy]

  include ResulsPagination

  # GET /employees
  def index
    @employees = Employee
    .limit(items_per_page)
    .offset(resuls_offset)

    if params[:term].present?
      @employees.where!('first_name ilike :term OR last_name ilike :term', term: "%#{ params[:term] }%")
    end

    render json: @employees
  end

  # GET /employees/1
  def show
    render json: @employee
  end

  # POST /employees
  def create
    @employee = Employee.new(employee_params)

    if @employee.save
      render json: @employee, status: :created, location: @employee
    else
      render json: @employee.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /employees/1
  def update
    if @employee.update(employee_params)
      render json: @employee
    else
      render json: @employee.errors, status: :unprocessable_entity
    end
  end

  # DELETE /employees/1
  def destroy
    @employee.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_employee
      @employee = Employee.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def employee_params
      params.require(:employee).permit(:first_name, :last_name, :company_id)
    end
end
