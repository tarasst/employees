module ResulsPagination

   private

   def items_per_page
     10
   end

   def resuls_offset
     items_per_page * page_index
   end

   def page_index
     (params[:page] || 1).to_i - 1
   end


end
