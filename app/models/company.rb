class Company < ApplicationRecord
  has_many :employees

  validates :name, presence: true
  validates :name, uniqueness: true

  def as_json(options={})
    attributes.slice('id', 'name')
  end
end
