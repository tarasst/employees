class Employee < ApplicationRecord
  belongs_to :company

  validates :first_name, :last_name, :company_id, presence: true
  validates :first_name, uniqueness: { scope: :last_name }

  def as_json(options={})
    attributes.slice('id', 'first_name', 'last_name').merge(company: company)
  end
end
