# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

require 'faker'
require 'open-uri'

  Employee.delete_all
  Company.delete_all

  open('https://randomuser.me/api?results=1000') do |f|
     json = JSON.parse(f.read)
     json['results'].each do |r|
       name = r['name']
       next if name.blank?
       company = Company.find_or_create_by(name: Faker::Company.name)

       Employee.create(first_name: name['first'], last_name: name['last'], company: company)
       print '.'
     end
     puts 'done'
  end
