class CreateCompanies < ActiveRecord::Migration[5.2]
  def change
    enable_extension 'pgcrypto'
    create_table :companies, id: :uuid do |t|
      t.string :name

      t.timestamps
    end
  end
end
