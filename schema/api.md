
## <a name="resource-company">Company</a>

Stability: `prototype`

Company inventory

### Attributes

| Name | Type | Description | Example |
| ------- | ------- | ------- | ------- |
| **id** | *uuid* | unique identifier of company | `"01234567-89ab-cdef-0123-456789abcdef"` |
| **name** | *string* | unique name of company | `"example"` |

### <a name="link-POST-company-/companies">Company Create</a>

Create a new company.

```
POST /companies
```

#### Required Parameters

| Name | Type | Description | Example |
| ------- | ------- | ------- | ------- |
| **name** | *string* | unique name of company | `"example"` |



#### Curl Example

```bash
$ curl -n -X POST https://shielded-lowlands-74046.herokuapp.com/companies \
  -d '{
  "name": "example"
}' \
  -H "Content-Type: application/json"
```


#### Response Example

```
HTTP/1.1 201 Created
```

```json
{
  "id": "01234567-89ab-cdef-0123-456789abcdef",
  "name": "example"
}
```

### <a name="link-DELETE-company-/companies/{(%23%2Fdefinitions%2Fcompany%2Fdefinitions%2Fidentity)}">Company Delete</a>

Delete an existing company.

```
DELETE /companies/{company_id_or_name}
```


#### Curl Example

```bash
$ curl -n -X DELETE https://shielded-lowlands-74046.herokuapp.com/companies/$COMPANY_ID_OR_NAME \
  -H "Content-Type: application/json"
```


#### Response Example

```
HTTP/1.1 200 OK
```

```json
{
  "id": "01234567-89ab-cdef-0123-456789abcdef",
  "name": "example"
}
```

### <a name="link-GET-company-/companies/{(%23%2Fdefinitions%2Fcompany%2Fdefinitions%2Fidentity)}">Company Info</a>

Info for existing company.

```
GET /companies/{company_id_or_name}
```


#### Curl Example

```bash
$ curl -n https://shielded-lowlands-74046.herokuapp.com/companies/$COMPANY_ID_OR_NAME
```


#### Response Example

```
HTTP/1.1 200 OK
```

```json
{
  "id": "01234567-89ab-cdef-0123-456789abcdef",
  "name": "example"
}
```

### <a name="link-GET-company-/companies">Company List</a>

List existing companies.

```
GET /companies
```


#### Curl Example

```bash
$ curl -n https://shielded-lowlands-74046.herokuapp.com/companies
```


#### Response Example

```
HTTP/1.1 200 OK
```

```json
[
  {
    "id": "01234567-89ab-cdef-0123-456789abcdef",
    "name": "example"
  }
]
```

### <a name="link-PATCH-company-/companies/{(%23%2Fdefinitions%2Fcompany%2Fdefinitions%2Fidentity)}">Company Update</a>

Update an existing company.

```
PATCH /companies/{company_id_or_name}
```

#### Required Parameters

| Name | Type | Description | Example |
| ------- | ------- | ------- | ------- |
| **name** | *string* | unique name of company | `"example"` |



#### Curl Example

```bash
$ curl -n -X PATCH https://shielded-lowlands-74046.herokuapp.com/companies/$COMPANY_ID_OR_NAME \
  -d '{
  "name": "example"
}' \
  -H "Content-Type: application/json"
```


#### Response Example

```
HTTP/1.1 200 OK
```

```json
{
  "id": "01234567-89ab-cdef-0123-456789abcdef",
  "name": "example"
}
```


## <a name="resource-employee">Employee</a>

Stability: `prototype`

Employee

### Attributes

| Name | Type | Description | Example |
| ------- | ------- | ------- | ------- |
| **company:id** | *uuid* |  | `"01234567-89ab-cdef-0123-456789abcdef"` |
| **company:name** | *string* |  | `"example"` |
| **first_name** | *string* | first name of employee | `"example"` |
| **id** | *uuid* | unique identifier of employee | `"01234567-89ab-cdef-0123-456789abcdef"` |
| **last_name** | *string* | last name of employee | `"example"` |

### <a name="link-POST-employee-/employees">Employee Create</a>

Create a new employee.

```
POST /employees
```

#### Required Parameters

| Name | Type | Description | Example |
| ------- | ------- | ------- | ------- |
| **company_id** | *uuid* | identifier of company | `"01234567-89ab-cdef-0123-456789abcdef"` |
| **first_name** | *string* | first name of employee | `"example"` |
| **last_name** | *string* | last name of employee | `"example"` |



#### Curl Example

```bash
$ curl -n -X POST https://shielded-lowlands-74046.herokuapp.com/employees \
  -d '{
  "first_name": "example",
  "last_name": "example",
  "company_id": "01234567-89ab-cdef-0123-456789abcdef"
}' \
  -H "Content-Type: application/json"
```


#### Response Example

```
HTTP/1.1 201 Created
```

```json
{
  "id": "01234567-89ab-cdef-0123-456789abcdef",
  "first_name": "example",
  "last_name": "example",
  "company": {
    "id": "01234567-89ab-cdef-0123-456789abcdef",
    "name": "example"
  }
}
```

### <a name="link-DELETE-employee-/employees/{(%23%2Fdefinitions%2Femployee%2Fdefinitions%2Fidentity)}">Employee Delete</a>

Delete an existing employee.

```
DELETE /employees/{employee_id}
```


#### Curl Example

```bash
$ curl -n -X DELETE https://shielded-lowlands-74046.herokuapp.com/employees/$EMPLOYEE_ID \
  -H "Content-Type: application/json"
```


#### Response Example

```
HTTP/1.1 200 OK
```

```json
{
  "id": "01234567-89ab-cdef-0123-456789abcdef",
  "first_name": "example",
  "last_name": "example",
  "company": {
    "id": "01234567-89ab-cdef-0123-456789abcdef",
    "name": "example"
  }
}
```

### <a name="link-GET-employee-/employees/{(%23%2Fdefinitions%2Femployee%2Fdefinitions%2Fidentity)}">Employee Info</a>

Info for existing employee.

```
GET /employees/{employee_id}
```


#### Curl Example

```bash
$ curl -n https://shielded-lowlands-74046.herokuapp.com/employees/$EMPLOYEE_ID
```


#### Response Example

```
HTTP/1.1 200 OK
```

```json
{
  "id": "01234567-89ab-cdef-0123-456789abcdef",
  "first_name": "example",
  "last_name": "example",
  "company": {
    "id": "01234567-89ab-cdef-0123-456789abcdef",
    "name": "example"
  }
}
```

### <a name="link-GET-employee-/employees">Employee List</a>

List existing employees.

```
GET /employees
```


#### Curl Example

```bash
$ curl -n https://shielded-lowlands-74046.herokuapp.com/employees
```


#### Response Example

```
HTTP/1.1 200 OK
```

```json
[
  {
    "id": "01234567-89ab-cdef-0123-456789abcdef",
    "first_name": "example",
    "last_name": "example",
    "company": {
      "id": "01234567-89ab-cdef-0123-456789abcdef",
      "name": "example"
    }
  }
]
```

### <a name="link-PATCH-employee-/employees/{(%23%2Fdefinitions%2Femployee%2Fdefinitions%2Fidentity)}">Employee Update</a>

Update an existing employee.

```
PATCH /employees/{employee_id}
```

#### Required Parameters

| Name | Type | Description | Example |
| ------- | ------- | ------- | ------- |
| **company_id** | *uuid* | identifier of company | `"01234567-89ab-cdef-0123-456789abcdef"` |
| **first_name** | *string* | first name of employee | `"example"` |
| **last_name** | *string* | last name of employee | `"example"` |



#### Curl Example

```bash
$ curl -n -X PATCH https://shielded-lowlands-74046.herokuapp.com/employees/$EMPLOYEE_ID \
  -d '{
  "first_name": "example",
  "last_name": "example",
  "company_id": "01234567-89ab-cdef-0123-456789abcdef"
}' \
  -H "Content-Type: application/json"
```


#### Response Example

```
HTTP/1.1 200 OK
```

```json
{
  "id": "01234567-89ab-cdef-0123-456789abcdef",
  "first_name": "example",
  "last_name": "example",
  "company": {
    "id": "01234567-89ab-cdef-0123-456789abcdef",
    "name": "example"
  }
}
```


