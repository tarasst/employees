# Employees app

Employees and their Companies inventory web application.

Basis for API design is built second
[HTTP+JSON API Design Guide](https://geemus.gitbooks.io/http-api-design/content/en/)


## API design considerations

 * uuid as object identities
 * [nested relations](https://geemus.gitbooks.io/http-api-design/content/en/responses/nest-foreign-key-relations.html)
 * machine-readable JSON schema: [schema/api.json](schema/api.json)
 * human-readable documentation with executable examples. Please see: [schema/api.md](schema/api.md)


## Demo

Please see: [Employees (deployed on Heroku)](https://shielded-lowlands-74046.herokuapp.com/)

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

To get it running in local environment you need: Mac OS or Linux, Docker


### Installing

```sh
git clone https://gitlab.com/tarasst/employees.git

cd employees

docker-compose up db # spins-up the Postgresql container
# in a second terminal window:
docker-compose run web bundle install # installs gems
docker-compose run web bin/rake db:setup # creates and seed database
docker-compose up web

```

### Changing the JSON API

Change the definitions for [schemata/company.json](schemata/company.json) and/or [schemata/employee.json](schemata/employee.json).

After changes are saved, re-generate the main schema:

```sh
docker-compose run web bin/rake api-schema
```

This also will update accordingly [The API Documentation](schema/api.md)

To debug schema, find other `rake` tasks in [Rakefile](Rakefile)


## Running the tests

```sh
docker-compose run web bin/rspec

```


## Deployment

To deploy on Heroku please follow [Getting Started on Heroku with Rails 5.x](https://devcenter.heroku.com/articles/getting-started-with-rails5)

To seed database:
```sh
heroku run rake db:seed
```

## Built With

* [Ruby on Rails](https://rubyonrails.org/) - The web framework
* [prmd](https://github.com/interagent/prmd) - JSON schema tool
* [committee](https://github.com/interagent/committee) - middleware to build services with JSON Schema
* [Vue.js](https://vuejs.org/) - Javascript framework
* [Docker](https://docs.docker.com/) - runs applications isolated in containers
* [RSpec](http://rspec.info/) - testing framework


## Authors

* **Taras Struk** - *Initial work* - [tarasst](https://gitlab.com/tarasst)
