
# Read https://github.com/interagent/committee

driver = Committee::Drivers::HyperSchema.new
schema = driver.parse(JSON.parse(File.read(Rails.root.join('schema/api.json'))))

Rails.application.config.middleware.use(Committee::Middleware::RequestValidation,
  schema: schema
  # prefix: '/api'
)

Rails.application.config.middleware.use(Committee::Middleware::ResponseValidation,
  schema: schema,
  prefix: '/api'
)
