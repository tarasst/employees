Rails.application.routes.draw do

  resources :employees
  resources :companies

  # permits to request directly API enpoints w/o validation in development mode
  scope '/debug' do
    resources :employees
    resources :companies
  end if Rails.env.development?

  namespace :explore do
    resources :employees, only: [:index, :show]
    resources :companies, only: [:index, :show]
  end

  root to: 'explore/employees#index'

end
